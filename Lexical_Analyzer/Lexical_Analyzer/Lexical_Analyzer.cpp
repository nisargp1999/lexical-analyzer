// Program_1.cpp : Defines the entry point for the console application.
//

//Assumption: Every /* or */ means the whole line is a comment
//Assumption: Numbers are int


#include "stdafx.h"
#include <string>;
#include <sstream>;
#include <fstream>;
#include <iostream>;
#include <vector>;
#include <stdio.h>;
#include <stdlib.h>;
#include <ctype.h>;



using namespace std;

//For Token Table Vector
struct TokenTable {
	string token;
	string className;
	int index;
};

//For Number Table
struct Number {
	string num;
	string attribute;
};

bool isKeyword(string k, vector<string> keyword);		//checks if string k is in vector keyword
bool isSymbol(string c, vector<string> symbol);			//checks if string c is in vector symbol
bool isId(string id, vector<string> idVector);			//checks if string id is already in vector idVector
bool isThisNum(string num);								//checks  if string num is a number
bool isNum(string num, vector<Number> numVector);		//checks if string num is already in vector numVector
vector<TokenTable> token;								//Token vector
bool isInt(string num);									//checks if string num is an integer number
int findVal(string word, vector<string> thisVector);	//finds an index of string word located in vector thisVector


int main()
{
	//Input and output files
	ifstream fin;
	ofstream fout;
	fout.open("output.txt");
	fin.open("input.txt");

	string temp;	//stores getline

					//Check if file can be opened
	if (!fin) {
		cout << "Unable to open file input.txt";
		exit(1);   // call system to stop
	}

	//.............................................................................................

	vector<string> words;
	string word;
	vector<string> symbols = { "+", "-", "*", "/", "<", ">", "<=", ">=", "==", "!=", "=", ";", ",", "(", ")", "[", "]", "{", "}", "/*", "*/" };		//symbols vector
	vector<string> keyword = { "else", "if", "int", "return", "void", "while" };			//keyword vector
	vector<Number> number;		//number vector
	vector<string> id;			//identifier vector
	vector<TokenTable> token;

	fout << "CODE WITH COMMENTS STRIPPED OFF:" << endl;
	fout << "________________________________" << endl;
	while (fin.good()) {
		getline(fin, temp);
		while (temp.find("/*") != string::npos || temp.find("*/") != string::npos)		//while comment
		{
			getline(fin, temp);			//skip current line and go to next
		}
		istringstream iss(temp);		//Scans the string temp
		fout << temp << endl;
		while (iss >> word)				//Goes through all words in current line separately
		{
			words.push_back(word);		//Inserts current word in vector words

			//if word is identifier and not already in id vector
			if (!isKeyword(word, keyword) && !isSymbol(word, symbols) && !isId(word, id) && !isThisNum(word)) {
				id.push_back(word);
				TokenTable tempToken = { word, "id", id.size() - 1 };
				token.push_back(tempToken);
			}//Inserts word in identifier vector and token table
			 //if word is identifier but already in id vector
			else if (!isKeyword(word, keyword) && !isSymbol(word, symbols) && !isThisNum(word)) {
				TokenTable tempToken = { word, "id", findVal(word, id) };
				token.push_back(tempToken);
			}//Inserts word in token table only

			 //if word is keyword
			if (isKeyword(word, keyword) && findVal(word, keyword) != NULL)
			{
				TokenTable tempToken = { word, "kw", findVal(word, keyword) };
				token.push_back(tempToken);
			}//Inserts word in token table

			 //if word is symbol
			if (isSymbol(word, symbols) && findVal(word, symbols)) {
				TokenTable tempToken = { word, "symbol", findVal(word, symbols) };
				token.push_back(tempToken);
			}//Inserts word in token table

			 //if word is number
			if (isThisNum(word) && !isNum(word, number))
			{
				if (isInt(word)) {		//if number is an integer
					Number tempObject = { word, "int" };
					number.push_back(tempObject);
					TokenTable tempToken = { word, "num", number.size() - 1 };
					token.push_back(tempToken);
				}
				else {					//if number if a flot
					Number tempObject = { word, "float" };
					number.push_back(tempObject);
					TokenTable tempToken = { word, "num", number.size() - 1 };
					token.push_back(tempToken);
				}
			}//Inserts number in token table with appropriate type


		}
	}



	//Print Keyword Table
	fout << endl << "Keywords\t" << "Index" << endl;
	fout << "________\t_____" << endl;
	for (int i = 0; i < keyword.size(); i++)
	{
		fout << keyword[i] << "\t\t" << i << endl;
	}
	fout << endl;
	//.................................................

	//Print Symbol Table
	fout << "Symbols\t\t" << "Index" << endl;
	fout << "_______\t\t_____" << endl;
	for (int i = 0; i < symbols.size(); i++)
	{
		fout << symbols[i] << "\t\t" << i << endl;
	}
	fout << endl;
	//.................................................

	//Print Identifier Table
	fout << "Identifiers\t" << "Index" << endl;
	fout << "___________\t_____" << endl;
	for (int i = 0; i < id.size(); i++)
	{

		fout << id[i] << "\t\t" << i << endl;
	}
	fout << endl;


	//Print Number Table
	fout << "Number\t\t" << "Index\t\t" << "Attribute" << endl;
	fout << "______\t\t_____\t\t_________" << endl;
	for (int i = 0; i < number.size(); i++)
	{
		fout << number[i].num << "\t\t" << i << "\t\t" << number[i].attribute << endl;
	}
	fout << endl;
	//..................................................

	//Print Token Table
	fout << "Token\t\t" << "Class\t\t" << "Index" << endl;
	fout << "_____\t\t_____\t\t_____" << endl;
	for (int i = 0; i < token.size(); i++)
	{
		fout << token[i].token << "\t\t" << token[i].className << "\t\t" << token[i].index << endl;
	}
	//..................................................


	




	//Close all files
	fin.close();
	fout.close();
	system("pause");
	return 0;
}


bool isSymbol(string c, vector<string> symbol)
{
	bool result = false;

	for (int i = 0; i < symbol.size(); i++) {
		if (symbol[i] == c)
		{
			result = true;
		}
	}
	return result;
}

bool isKeyword(string k, vector<string> keyword)
{
	bool result = false;

	for (int i = 0; i < keyword.size(); i++) {
		if (keyword[i] == k)
		{
			result = true;
		}
	}
	return result;
}

bool isId(string id, vector<string> idVector)
{
	bool result = false;

	for (int i = 0; i < idVector.size(); i++) {
		if (idVector[i] == id)
		{
			result = true;
		}
	}

	for (int i = 0; i < id.length(); i++)
	{
		if (!isalpha(id[i]))
		{
			result = false;
		}
	}

	
	return result;
}

bool isThisNum(string num)
{
	bool result = false;

	for (int i = 0; i < num.size(); i++)
	{
		if (isdigit(num[i])) {
			result = true;
		}
	}

	return result;
}

bool isNum(string num, vector<Number> numVector)
{
	bool result = false;

	for (int i = 0; i < numVector.size(); i++) {
		if (numVector[i].num == num)
		{
			result = true;
		}
	}
	return result;
}

bool isInt(string num)
{
	bool result = true;

	for (int i = 0; i < num.size(); i++) {
		if (num[i] == '.') {
			result = false;
		}
	}
	return result;
}

int findVal(string word, vector<string> thisVector)
{
	int result = NULL;
	for (int i = 0; i < thisVector.size(); i++)
	{
		if (word == thisVector[i])
		{
			result = i;
		}
	}
	return result;
}